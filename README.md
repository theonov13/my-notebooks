[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/theonov13%2Fmy-notebooks/master)

# my-notebooks

# mybinder 

Make your notebooks available. 

- make a repo somewhere (GitHub or GitLab)  
- put your notebooks inside the repo   
- prepare a requirements.txt    
- go to https://mybinder.org/   
- copy the badge to your README.md 

# TODO
- think about DASHBOARDs    
        - [voila](https://github.com/voila-dashboards/voila)

- think about other ways to make notebooks available    
        - [heroku](https://www.heroku.com/)

- think about Python + Julia notebooks    
        - https://mybinder.readthedocs.io/en/latest/examples/sample_repos.html

- add other visualizations    
        - Python:nglview   
        - Julia:[Bio3DView](https://nbviewer.jupyter.org/github/jgreener64/Bio3DView.jl/blob/master/examples/tutorial.ipynb)   
